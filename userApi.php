<?php
   //input data
   $document_url=$_GET["url"];
   $compare = isset($_GET["compare"]) ? $_GET["compare"] : false;
   $search_type = isset($_GET["search_type"]) ? $_GET["search_type"] : -1; //1: search by size,width, height ; //2: search by mean status, width, height
   $html = isset($_GET["html"]) ? $_GET["html"] : false; //1: search by size,width, height ; //2: search by mean status, width, height

   //get file name
   $destination_folder = '/tmp';
   $split_point = '/';
   $result = array_map('strrev', explode($split_point, strrev($document_url)));
   $image_name = $result[0];
   $image_name= get_random_string(5) . "_" . $image_name;
   shell_exec('wget --continue '.$document_url . " -O " . $destination_folder."/" . $image_name . " -nv");
   
   //identify source image
   $img = identify_image($image_name, true);	
   $img_size = floatval($img["img_size"]);
   $width = intval($img["width"]);
   $height = intval($img["height"]);
   $mean_stat = floatval($img["mean_stat"]);

   //database query
   $m = new MongoClient("mongodb://fofreader:FoFRead3r!#@104.130.31.210:27017/AirbnbData");
   $collection = $m->AirbnbData->room_images;

   if($search_type>=0) {
    if($search_type==0)
        $query_filter = array("mean_stat" => $mean_stat, "width" => $width, "height" => $height);
    else
        $query_filter = array("img_size" => $img_size, "width" => $width, "height" => $height);
    
    $cursor = $collection->find($query_filter);

    //compare images
    if($compare) {
     $array = compare_images($cursor,$destination_folder,$image_name,true);
	 if($html) {
		draw_html($array, $document_url, $img);
	 } else {
		 echo "<pre>"; print_r($array);	 
	 }
    } else {
		print_r(json_encode($img));
	}
   }
   
   if($search_type == -1) {
	   print_r(json_encode($img));
   }

   //delete source image
   $command = "rm -rf " . $destination_folder."/".$image_name;
   shell_exec($command);


  //random string
  function get_random_string($length = 10) {
    $random_image_name = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, $length);
    return $random_image_name;
  }

  //identify image
  function identify_image($image_name, $print_debug=false) {
    $destination_folder = '/tmp';
    $command = "identify -format '%[height];%[width];%[size];%[mean];%[standard-deviation];%[date:create];%[date:modify]' " .$destination_folder."/".$image_name;
    $output = shell_exec($command);
    $data = explode(";", $output);
    $height=$data[0];
    $width=$data[1];
    $img_size=$data[2];
    $mean_stat=$data[3]; //CALCULATED: average value statistic of image
    $stdev=$data[4]; //CALCULATED: standard deviation statistic of image
    $final_arr=array("height"=>intval($height), "width"=>intval($width), "img_size"=>floatval($img_size), "mean_stat"=>floatval($mean_stat), "stdev"=>floatval($stdev));
    // if($print_debug) {
        // print_r(json_encode($final_arr));
    // }

    return $final_arr;
  }

  //sort array by image diff
  function sortByDiff($x, $y) {
    return $x['diff'] - $y['diff'];
  }

  //compare images
  function compare_images($cursor,$destination_folder,$source_image_name, $print_debug) {
    $compare_program_path = "/var/www/html/compare_images.sh";
    $i=0;
    $array = array();
    foreach ($cursor as $document) {
        //download image
        $random_image_name = get_random_string(15);
        if($document["aws"]==1 || $document["aws"]==true) {
            //get file name
            $split_point = '/';
            $result = array_map('strrev', explode($split_point, strrev($document["url"])));
            $parsed_url = explode('https://', $document["url"]);
            $amazon_url = 'https://friendorfraud.s3.amazonaws.com/'.$parsed_url[1];
            $download_url = $amazon_url;
        }
        else {
            $download_url = $document["url"];
        }

        $wget_command = 'wget --continue '.$download_url . " -O " . $destination_folder."/".$random_image_name . " -nv";
        shell_exec($wget_command);

        //execute compare command
        $command = $compare_program_path. " " .$destination_folder."/".$source_image_name . " " .$destination_folder."/".$random_image_name;
        $output_analysis = shell_exec($command);
        $output_analysis_tmp = explode(";", $output_analysis);
        $pixel_difference =  floatval(explode(": ", $output_analysis_tmp[0])[1]);
        $is_img_identical = intval($output_analysis_tmp[1]);

        $processed_array = array("id"=>$document["id"], "diff"=>$pixel_difference, "url"=>$download_url, "img_size"=>$document["img_size"], "height"=>$document["height"], "width"=>$document["width"], "mean_stat"=>$document["mean_stat"], "stdev"=>$document["stdev"]);
        $array[] = $processed_array;

        //delete downloaded file
        $commanddel = "rm -rf " . $destination_folder."/".$random_image_name;
        shell_exec($commanddel);
    }
    
    usort($array, 'sortByDiff');
	// if($print_debug) {
		// 
	// }
	
	return $array;
  }
  
  function draw_html($database_data, $original_img, $original_img_data) {
	  $html = '';
	  $html = "<html><head><style>
				img.resize{
				   max-width:100%;
				}
				.box2 {
				  display: inline-block;
				  width: 30%;
				  margin: 1em;
				}
				</style><title>ImageCompare results</title></head><body></br>";
				$html = $html . '<h1>Original image:</h1>';
				$html = $html . '<div><img src="'.$original_img.'"><br>';
				$html = $html . "Image size: ". $original_img_data["img_size"] ."</br>Height: " . $original_img_data["height"]."</br>Width: ".$original_img_data["width"]."</br>Stdev: ".$original_img_data["stdev"]."</br>Mean stat: ".$original_img_data["mean_stat"]."</p></div>";
				$html = $html . '<br><br><h1>Database data:</h1>';
	  $i=1;
	  foreach($database_data as $key => $value) {
		$html = $html . '<div class="box2"><img class="resize" src="'.$value["url"].'">';
		$html = $html . "<p> id: " . $value["id"] . "</br>Diff: ".$value["diff"]. "%</br>" . "Image size: ". $value["img_size"] ."</br>Height: " . $value["height"]."</br>Width: ".$value["width"]."</br>Stdev: ".$value["stdev"]."</br>Mean stat: ".$value["mean_stat"]."</p>";
		$html = $html . "</div>";
		$i++;
	  }
	  
	  $html = $html . "</body></html>";
	  echo $html;
  }
?>
