***** HOW TO *****
API is using imagemagic library and extracts several parameters from the image. 

Syntax:

************************************************
1.
************************************************
You can use API in a way that return json array of the processed image properties.
Input parameters (url)
* $url 

Example: 
  HTTP call in browser or curl call (no authentication)
  http://104.130.169.234/userApi.php?url=https://friendorfraud.s3.amazonaws.com/a0.muscache.com/im/pictures/6708335/b4750b57_original.jpg

  Output:
  {"height":683,"width":1024,"img_size":88.5,"mean_stat":39945.1,"stdev":14100.3}


**************************************************************************************
2.
*************************************************************************************
API returns list of similar images based on image properties (height, width, image statistics). Array property diff shows % of different pixels compared to reference
Input parameters (url, compare, search_type, html)
* $url 
* $compare [true/false]
* $html [true/false]
* $search_type [0/1] ... 
	-> 0: search similarity based on width, height, mean statistics and stdev of image (this is prefered use)
	-> 1: search similarity based on width, height, size
		
Example: 
  HTTP call in browser or curl call (no authentication), which shows 47 results, 8 images are identical
  http://104.130.169.234/userApi.php?url=https://friendorfraud.s3.amazonaws.com/a2.muscache.com/im/pictures/f367121e-8c9e-49e0-b04e-b2268cb6136e.jpg&compare=true&search_type=0&html=true
  
  Output:
	HTML with images
