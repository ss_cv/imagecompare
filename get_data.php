<?php
   $destination_folder = '/tmp';
   $m = new MongoClient("mongodb://fofreader:FoFRead3r!#@104.130.31.210:27017/AirbnbData", array("socketTimeoutMS" => "2240000"));
   $collection = $m->AirbnbData->room_images;
   $query_filter = array("is_processed"=>array('$in' => [null, false]));

   $now = DateTime::createFromFormat('U.u', microtime(true));
   $time_start = microtime(true);
   
   while(1) {
       $cursor = $collection->find($query_filter)->limit(1000);

       foreach ($cursor as $document) {
            $document_url = $document["url"];
            $newdata = array('$set' => array("is_processed"=>true));
            $collection->update(array("_id" => $document["_id"]), $newdata);
       }

       foreach ($cursor as $document) {
            $document_url = $document["url"];
            $newdata = array('$set' => array("is_processed"=>true));
            $collection->update(array("_id" => $document["_id"]), $newdata);

            //get file name
            $split_point = '/';
            $result = array_map('strrev', explode($split_point, strrev($document_url)));
            $image_name = $result[0];
            $parsed_url = explode('https://', $document_url);

            if($document["aws"]==1 || $document["aws"]==true) {
            //download image
              shell_exec('wget --continue --timestamping https://friendorfraud.s3.amazonaws.com/'.$parsed_url[1] . " -P " . $destination_folder . " -nv");
                }
            else {
              shell_exec('wget --continue --timestamping '.$document_url . " -P " . $destination_folder . " -nv");
            }

            //identify image
            /*
              See list of properties on:
              http://www.imagemagick.org/script/escape.php
                  $command = "identify -verbose " .$destination_folder."/".$image_name; //identify all properties (for debug)
            */
            $time_start1 = microtime(true);
            $command = "identify -format '%[height];%[width];%[size];%[mean];%[standard-deviation];%[date:create];%[date:modify]' " .$destination_folder."/".$image_name;
            $time_end1 = microtime(true);
                $execution_time1 = ($time_end1 - $time_start1);

                echo $execution_time1 . "\n";

            $output = shell_exec($command);
            $data = explode(";", $output);
            $height=$data[0];
            $width=$data[1];
            $img_size=$data[2];
            $mean_stat=$data[3]; //CALCULATED: average value statistic of image
            $stdev=$data[4]; //CALCULATED: standard deviation statistic of image
            $date_created = $data[5];
            $date_modify = $data[6];

            $newdata = array('$set' => array("height" => intval($height), "width" => intval($width), "img_size" => floatval($img_size),
            "mean_stat"=> floatval($mean_stat), "stdev"=>floatval($stdev), "date_created"=>$date_created, "date_modified"=>$date_modify,
            "is_processed"=>true));	
            $collection->update(array("_id" => $document["_id"]), $newdata);

            //delete image
            $command = "rm -rf " . $destination_folder."/".$image_name;
            shell_exec($command);
        }
    }

        $time_end = microtime(true);
        $execution_time = ($time_end - $time_start);
        echo $execution_time . "\n";
?>